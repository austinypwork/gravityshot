using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Events/ParticleGameEvent")]
public class ParticleGameEvent : GameEvent<ParticleData> {}