using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Events/IntGameEvent")]
public class IntGameEvent : GameEvent<int> {}