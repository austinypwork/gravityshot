using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Events/GameEvent")]
public class GameEvent : ScriptableObject {
    private List<IGameEventListener> Listeners = new List<IGameEventListener>();
    public void Raise() {
        for (int i = Listeners.Count - 1; i >= 0; i--) {
            Listeners[i].OnEventRaised();
        }
        //Debug.Log("I am raised.");
    }
    public void RegisterListener(IGameEventListener listener) {
        if (!Listeners.Contains(listener)) {
            Listeners.Add(listener);
        } else {
            Debug.LogWarning("Listener already registered.");
        }
    }
    public void UnregisterListener(IGameEventListener listener) {
        Listeners.Remove(listener);
    }
}
public class GameEvent<T> : ScriptableObject {
    private List<IGameEventListener<T>> Listeners = new List<IGameEventListener<T>>();
    public T Param;
    
    public void RegisterListener(IGameEventListener<T> listener) {
        if (!Listeners.Contains(listener)) {
            Listeners.Add(listener);
        } else {
            Debug.LogWarning("Listener already registered.");
        }
    }
    public void UnregisterListener(IGameEventListener<T> listener) {
        Listeners.Remove(listener);
    }
    public void Raise(T param) {
        for (int i = Listeners.Count - 1; i >= 0; i--) {
            Listeners[i].OnEventRaised(param);
            //Debug.Log(Listeners[i].ToString());
        }
        //Debug.Log("Events: " + this.name + "Param: " + param.ToString());
    }

}
public interface IGameEventListener {
    public void OnEventRaised();
}
public interface IGameEventListener<T> {
    public void OnEventRaised(T param);
}