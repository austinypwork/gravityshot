using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowPredictor : MonoBehaviour {
    
    [SerializeField]
    List<PlanetGravity> m_GravityFields = new List<PlanetGravity>();
    
    [SerializeField]
    float minSpeed = 1f, maxSpeed = 10f;
    
    [SerializeField]
    int iterations = 10, maxFrames = 30;

    void OnDrawGizmosSelected(){
        
        //DebugExtension.DrawCapsule(transform.position, transform.position + (Vector3.up * m_Height), Color.red, 0.5f);

        if(m_GravityFields.Count < 1){
            PlanetGravity[] gravs = FindObjectsOfType<PlanetGravity>();
            m_GravityFields = new List<PlanetGravity>(gravs);
        }

        float step = (maxSpeed - minSpeed) / iterations;
        
        for(float i = minSpeed; i < maxSpeed; i += step){
            Vector3 newPos = transform.position, lastPos = transform.position;
            Vector3 vel = transform.forward * i;

            for(int j = 0; j < maxFrames; j++){
                
                //Vector3 newVel = Vector3.zero;

                foreach(var gravityField in m_GravityFields){
                    Vector3 distanceVec = gravityField.transform.position - newPos;
                
                    if(distanceVec.sqrMagnitude > gravityField.DEBUG_Radius * gravityField.DEBUG_Radius)
                        continue;

                    Vector3 gravityDir = distanceVec.normalized;
                    gravityDir *= (PlanetGravity.G_CONSTANT * gravityField.mass) / (gravityField.transform.position - newPos).sqrMagnitude;
                    vel += gravityDir * Time.deltaTime;

                    //vel += newVel;
                }

                newPos += vel * Time.deltaTime;

                Gizmos.color = Color.magenta;
                Gizmos.DrawLine(lastPos, newPos);
                
                //lastPos = new Vector3(newPos);
                lastPos.x = newPos.x;
                lastPos.y = newPos.y;
                lastPos.z = newPos.z;
            }
        }

    }

    
}
