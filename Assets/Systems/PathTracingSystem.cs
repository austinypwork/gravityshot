using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathTracingSystem : MonoBehaviour {

    List<BallSpawner> m_Spawners;
    List<PathTracer> m_PathTracers = new List<PathTracer>();

    Transform m_PathParent;

    // List of path tracers
    
    [SerializeField]
    Transform m_PathTracerPF;

    [SerializeField]
    float m_Threshold = 0.2f;

    bool started = false;

    bool togglePaths = true;

    public void TogglePaths(){
        togglePaths = !togglePaths;
        m_PathParent.gameObject.SetActive(togglePaths);
    }

    // Start is called before the first frame update
    void Start(){
        m_Spawners = new List<BallSpawner>( FindObjectsOfType<BallSpawner>(true) );

        GameObject pathParent = new GameObject("PathTracerParent");
        m_PathParent = pathParent.transform;
        m_PathParent.parent = transform;

    }

    void LateStart(){
        int id = 0;

        foreach (var spawner in m_Spawners){
            foreach(var ball in spawner.ballPool){
                ball.id = id;

                //Debug.Log("Path found: " + ball.id);

                m_PathTracers.Add(Instantiate(m_PathTracerPF, transform.position, transform.rotation, m_PathParent.transform).GetComponent<PathTracer>());

                m_PathTracers[id].gameObject.SetActive(false);
                m_PathTracers[id].id = ball.id;
                m_PathTracers[id].init();
                m_PathTracers[id].reset();

                id++;
            }
        }
    }

    // Update is called once per frame
    void Update(){

        if(!started){
            started = true;
            LateStart();
        }
        //return;
        foreach (var spawner in m_Spawners){
            foreach(var ball in spawner.ballPool){
                PathTracer path = m_PathTracers[ball.id];

                if(!ball.gameObject.activeInHierarchy || !path.gameObject.activeSelf)
                    continue;

                if((ball.rigidBody.position - path.currentPosition).sqrMagnitude < m_Threshold)
                    continue;

                path.addPoint(ball.rigidBody.position);

            }
        }
    }
}
