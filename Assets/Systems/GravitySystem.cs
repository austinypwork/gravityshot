using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySystem : MonoBehaviour
{
    List<BallSpawner> m_Spawners;
    List<PlanetGravity> m_GravityFields;

    // Start is called before the first frame update
    void Start() {
        m_Spawners = new List<BallSpawner>( FindObjectsOfType<BallSpawner>(true) );
        m_GravityFields = new List<PlanetGravity>( FindObjectsOfType<PlanetGravity>(true) );

        TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default);
    }

    // Update is called once per frame
    void LateUpdate(){
        foreach(var spawner in m_Spawners){
            UpdateGravity(spawner);
        }
    }

    void UpdateGravity(BallSpawner spawner){
        foreach(var ball in spawner.ballPool){
            if(!ball.gameObject.activeInHierarchy || !ball.tossed)
                continue;

            foreach(var gravityField in m_GravityFields){
                if(!gravityField.gameObject.activeInHierarchy)
                    continue;

                Vector3 distanceVec = gravityField.transform.position - ball.transform.position;
                
                if(distanceVec.sqrMagnitude > gravityField.sphereCollider.radius * gravityField.sphereCollider.radius)
                    continue;

                Vector3 gravityDir = distanceVec.normalized;
                gravityDir *= (PlanetGravity.G_CONSTANT * gravityField.mass * ball.mass) / (gravityField.transform.position - ball.transform.position).sqrMagnitude;
                ball.rigidBody.velocity += gravityDir * Time.deltaTime;
            }
        }
    }
}
