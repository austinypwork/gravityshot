using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TeleportationZone : UnityEngine.XR.Interaction.Toolkit.XRBaseInteractable
{

    [SerializeField]
    InputActionAsset actionAsset;
    
    [SerializeField]
    UnityEngine.XR.Interaction.Toolkit.XRRayInteractor rayInteractor;

    [SerializeField]
    UnityEngine.XR.Interaction.Toolkit.TeleportationProvider m_TeleportationProvider;
    bool m_TeleportStart = false;

    // Start is called before the first frame update
    void Start(){
        if(m_TeleportationProvider is null){
            m_TeleportationProvider = FindObjectOfType<UnityEngine.XR.Interaction.Toolkit.TeleportationProvider>();
        }

        rayInteractor.velocity = 4f;
        
        var teleActive = actionAsset.FindActionMap("XRI LeftHand").FindAction("Teleport Mode Activate");
        teleActive.Enable();
        teleActive.performed += OnTeleportActive;

        var teleCancel = actionAsset.FindActionMap("XRI LeftHand").FindAction("Teleport Mode Cancel");
        teleCancel.Enable();
        teleCancel.performed += OnTeleportCancel;

    }

    void OnTeleportActive(InputAction.CallbackContext context){
        m_TeleportStart = true;
        rayInteractor.lineType = UnityEngine.XR.Interaction.Toolkit.XRRayInteractor.LineType.ProjectileCurve;
        Debug.Log("Start teleport");
    }

    void OnTeleportCancel(InputAction.CallbackContext context){
        Debug.Log("Stop teleport");
        rayInteractor.lineType = UnityEngine.XR.Interaction.Toolkit.XRRayInteractor.LineType.StraightLine;

        if(m_TeleportStart){
            if(!rayInteractor.TryGetCurrent3DRaycastHit(out RaycastHit hit)){
                m_TeleportStart = false;
                return;
            }

            UnityEngine.XR.Interaction.Toolkit.TeleportRequest _request = new UnityEngine.XR.Interaction.Toolkit.TeleportRequest(){
                destinationPosition = hit.point
            };

            m_TeleportationProvider.QueueTeleportRequest(_request);
        }
        
        m_TeleportStart = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
