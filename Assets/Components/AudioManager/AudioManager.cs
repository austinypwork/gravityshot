using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public List<AudioClip> audioClips;
    private AudioSource m_AudioSource;
    private bool m_ClipFinished = false;

    // Start is called before the first frame update
    void Start(){
        m_AudioSource = GetComponent<AudioSource>();

        if(audioClips.Count < 1)
            return;
        
        StartCoroutine(SetRandomClip());
    }

    IEnumerator SetRandomClip(){
        m_ClipFinished = false;
        m_AudioSource.clip = audioClips[Random.Range(0, audioClips.Count)];
        m_AudioSource.Play();
        yield return new WaitForSeconds(m_AudioSource.clip.length);
        m_ClipFinished = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(m_ClipFinished)
            StartCoroutine(SetRandomClip());
    }
}
