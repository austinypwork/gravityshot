using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class PathTracer : MonoBehaviour, IGameEventListener<int> {
    public int id = 0;
    public Vector3 currentPosition = new Vector3(0f, 0f, 0f);
    LineRenderer m_LineRenderer;

    [SerializeField]
    private GameEvent<int> m_TossedEvent;

    [SerializeField]
    int m_MaxSize = 300;
    int m_Index = 0;

    public void init(){
        m_LineRenderer = GetComponent<LineRenderer>();
        m_LineRenderer.positionCount = m_MaxSize;

        if(!m_TossedEvent)
            m_TossedEvent = new GameEvent<int>();

        m_TossedEvent.RegisterListener(this);
    }

    public void reset(){
        m_Index = 0;
        
        for(int i = 0; i < m_MaxSize; i++){
            m_LineRenderer.SetPosition(i, Vector3.zero);
        }

        currentPosition = Vector3.zero;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnDestroy() {
        m_TossedEvent.UnregisterListener(this);
    }

    public void OnEventRaised(int ballId){
        if(id != ballId)
            return;

        gameObject.SetActive(true);
        reset();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addPoint(Vector3 pos){
        if(m_Index >= m_MaxSize)
            return;

        m_LineRenderer.SetPosition(m_Index, pos);
        currentPosition = m_LineRenderer.GetPosition(m_Index);

        m_Index++;

        for(int i = m_Index; i < m_MaxSize; i++){
            m_LineRenderer.SetPosition(i, pos);
        }

    }
}
