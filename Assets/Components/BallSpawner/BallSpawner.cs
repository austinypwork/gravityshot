using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    [SerializeField]
    private Transform m_ObjectToSpawn;
    
    [SerializeField]
    private Transform m_SpawnPoint;

    private Transform m_CurrentObject;
    
    [SerializeField]
    private float m_Threshold = 1f;

    [SerializeField]
    private bool m_DEBUG = false;

    [SerializeField]
    private DEBUG_Menu m_DebugMenu;

    public List<ThrowingBall> ballPool;
    int ballPoolIndex = 0;

    // Start is called before the first frame update
    void Start() {
        if(!m_DebugMenu)
            m_DebugMenu = FindObjectOfType<DEBUG_Menu>();

        for(int i = 0; i < 50; i++){
            ballPool.Add(Instantiate(m_ObjectToSpawn, m_SpawnPoint.position, m_SpawnPoint.rotation, transform).GetComponent<ThrowingBall>());
            ballPool[i].gameObject.SetActive(false);
        }

        spawnObject();
    }

    // Update is called once per frame
    void Update(){
        if((m_CurrentObject.position - m_SpawnPoint.position).sqrMagnitude > m_Threshold){
            spawnObject();
        }
    }

    private void spawnObject(){
        ballPool[ballPoolIndex].init(m_SpawnPoint.position);
        ballPool[ballPoolIndex].gameObject.SetActive(true);

        m_CurrentObject = ballPool[ballPoolIndex].transform;

        if(m_DEBUG){
            ballPool[ballPoolIndex].useSpeedBoost = true;
            ballPool[ballPoolIndex].speedBoost = m_DebugMenu.currentSpeed;
        }
        
        ballPoolIndex++;

        if(ballPoolIndex >= ballPool.Count)
            ballPoolIndex = 0;
    }
}
