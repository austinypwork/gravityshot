using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DEBUG_Menu : MonoBehaviour
{
    [SerializeField]
    private Slider speedSlider, gravitySlider;

    [SerializeField]
    private TMPro.TextMeshProUGUI speedText, gravityText;

    private float MAX_SPEED = 10f;
    private float MAX_GRAVITY = 0.5f;

    public float currentSpeed = 1f, currentGravity = 0.006f;

    void Start() {
        OnSpeedSliderChange();
        OnGravitySliderChange();
    }

    public void OnSpeedSliderChange(){
        currentSpeed = speedSlider.value * MAX_SPEED;
        speedText.text = "Speed: " + currentSpeed;
    }

    public void OnGravitySliderChange(){
        currentGravity = Mathf.Clamp(gravitySlider.value * MAX_GRAVITY, 0.001f, MAX_GRAVITY);
        gravityText.text = "Gravity: " + currentGravity;
        PlanetGravity.G_CONSTANT = currentGravity;
    }

    public void OnReset(){
        currentGravity = 0;
        currentSpeed = 0;

        speedSlider.value = 0;
        gravitySlider.value = 0;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
