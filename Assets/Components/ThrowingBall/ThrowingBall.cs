using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowingBall : MonoBehaviour {

    public int id = 0;

    [SerializeField]
    private GameEvent<int> m_TossedEvent;

    [SerializeField]
    GameEvent<ParticleData> m_ParticleEvent;

    public Rigidbody rigidBody;
    public float mass = 1f;

    [SerializeField]
    private Transform m_TrackerFocus, m_Tracker;

    [SerializeField]
    private float m_TrackerDistance = 1f;

    private string m_HubTag = "hub";

    [SerializeField]
    private int SPEED = 10;
    
    public float speedBoost = 1f;
    public bool useSpeedBoost = false;

    public bool tossed = false;

    private float m_DespawnDistance = 50f;

    [SerializeField]
    private bool DEBUG = false;

    [SerializeField]
    private TMPro.TextMeshProUGUI m_TrackerText;

    private float m_TrackerLifespan = 5f;

    public void init(Vector3 pos){
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;
        transform.position = pos;
        transform.eulerAngles = Vector3.zero;

        tossed = false;
        m_TrackerLifespan = 5f;
        m_Tracker.gameObject.SetActive(false);
        GetComponent<TrailRenderer>().Clear();

        spawnParticle();

        if(DEBUG)
            rigidBody.AddForce(transform.forward * SPEED, ForceMode.Impulse);
    }

    void spawnParticle(){
        ParticleData particleData = new ParticleData();

        particleData.position = transform.position;
        particleData.scale = new Vector3(.1f, .1f, .1f);
        particleData.color = Color.cyan;
        particleData.type = ParticleType.SPARKS;
        
        m_ParticleEvent.Raise(particleData);
    }

    void Start() {
        if(!m_TossedEvent)
            m_TossedEvent = new GameEvent<int>();

        if(!m_ParticleEvent)
            m_ParticleEvent = new GameEvent<ParticleData>();

        if(m_TrackerFocus == null)
            m_TrackerFocus = Camera.main.transform;

        if(rigidBody == null)
            rigidBody = GetComponent<Rigidbody>();

        rigidBody.useGravity = false;
        
        if(DEBUG)
            rigidBody.AddForce(transform.forward * SPEED, ForceMode.Impulse);
    }

    void Update() {
        updateTracker();
        
        if(rigidBody.position.sqrMagnitude > m_DespawnDistance * m_DespawnDistance)
            this.gameObject.SetActive(false);
    }

    void updateTracker(){
        if(!tossed || !m_Tracker.gameObject.activeInHierarchy)
            return;

        m_Tracker.position = m_TrackerFocus.position + ((transform.position - m_TrackerFocus.position).normalized * m_TrackerDistance);
        m_Tracker.LookAt(transform.position);

        m_TrackerText.text = "Distance: " + (int)(transform.position - m_TrackerFocus.position).magnitude + "km";

        m_TrackerLifespan -= Time.deltaTime;

        if(m_TrackerLifespan < 0)
            m_Tracker.gameObject.SetActive(false);
    }

    private void OnCollisionExit(Collision other) {
        
    }

    private void OnTriggerExit(Collider other) {
        //Debug.Log("Trigger Exit: " + other.gameObject.name);
        if(tossed)
            return;

        if(other.gameObject.CompareTag(m_HubTag)){
            Debug.Log("Exit Hub");
            rigidBody.useGravity = false;
            m_Tracker.gameObject.SetActive(true);
            tossed = true;

            if(useSpeedBoost)
                rigidBody.AddForce(rigidBody.velocity.normalized * speedBoost, ForceMode.Impulse);

            m_TossedEvent.Raise(id);

        }
    }
}
