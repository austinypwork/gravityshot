using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetBody : MonoBehaviour
{

    [SerializeField]
    GameEvent<ParticleData> m_ParticleEvent;

    // Start is called before the first frame update
    void Start()
    {
        if(!m_ParticleEvent)
            m_ParticleEvent = new GameEvent<ParticleData>();
    }

    void OnCollisionEnter(Collision other){
        if(other.gameObject.CompareTag("ball")){
            Debug.Log("Spawning Particle");
            
            other.gameObject.SetActive(false);

            ParticleData particleData = new ParticleData();

            particleData.position = other.contacts[0].point;
            particleData.scale = new Vector3(.1f, .1f, .1f);
            particleData.color = Color.red;
            particleData.type = ParticleType.SPARKS;
            
            m_ParticleEvent.Raise(particleData);
        }
    }
}
