using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGravity : MonoBehaviour {
    
    public float mass = 1000f;
    public static float G_CONSTANT = 0.006f;
    private Transform m_Transform;
    public SphereCollider sphereCollider;
    public float DEBUG_Radius = 10;
    
    // Start is called before the first frame update
    void Start(){
        m_Transform = transform;
        sphereCollider = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update(){
        
    }
    
    private void LateUpdate() {
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.CompareTag("ball")){
            Debug.Log("Enter Gravity");
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.gameObject.CompareTag("ball")){
            Debug.Log("Exit Gravity");
        }
    }
}
