using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotation : MonoBehaviour
{

    [SerializeField]
    Vector3 m_LookAtDir = new Vector3(0,0,1);

    [SerializeField]
    float m_RotationSpeed = 100f;

    [SerializeField]
    bool m_Randomize = false;

    // Start is called before the first frame update
    void Start(){
        if(m_Randomize){
            m_LookAtDir.x = Random.Range(-100, 100);
            m_LookAtDir.y = Random.Range(-100, 100);
            m_LookAtDir.z = Random.Range(-100, 100);
            transform.LookAt(transform.position + m_LookAtDir);
        }
        
    }

    // Update is called once per frame
    void FixedUpdate(){
        transform.Rotate(Vector3.up, m_RotationSpeed * Time.deltaTime);
    }
}
