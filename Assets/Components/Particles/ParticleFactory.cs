using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ParticleType {
    SPARKS
};

public struct ParticleData {
    public Vector3 position, scale;
    public Color color;
    public ParticleType type;
    
};

public class ParticleFactory : MonoBehaviour, IGameEventListener<ParticleData>
{
    
    [SerializeField]
    GameObject m_SparksParticle;

    [SerializeField]
    GameEvent<ParticleData> m_ParticleEvent;

    void Start(){
        m_ParticleEvent.RegisterListener(this);
    }

    void OnDestroy(){
        m_ParticleEvent.UnregisterListener(this);
    }

    public void OnEventRaised(ParticleData param){
        GameObject particle = new GameObject();

        switch(param.type){
            case ParticleType.SPARKS:
                particle = Instantiate(m_SparksParticle, param.position, transform.rotation, transform);
                break;
        }

        particle.transform.position = param.position;
        var main = particle.GetComponent<ParticleSystem>().main;
        main.startColor = param.color;
    }

}
